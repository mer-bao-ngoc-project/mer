import React from 'react';
import Social from './Instagram/Social'
import InstagramPostList from './Instagram/InstagramPostList'

function Instagram() {
    return (
        <div className="axil-instagram-area axil-section-gap bg-color-grey">
            <div className="container">
                {/* Start Social Area  */}
                <Social/>
                {/* End Social Area  */}
                <div className="row">
                    <div className="col-lg-12">
                        <div className="section-title">
                            <h2 className="title">Instagram</h2>
                        </div>
                    </div>
                </div>
                {/* Start instagram-post-list Area  */}
                <InstagramPostList/>
                {/* End instagram-post-list Area  */}

            </div>
        </div>
    );
}

export default Instagram;

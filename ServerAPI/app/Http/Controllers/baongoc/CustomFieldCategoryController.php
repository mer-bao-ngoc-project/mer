<?php

namespace App\Http\Controllers\baongoc;

use App\Http\Controllers\Controller;
use App\Models\CustomFieldCategory;
use Illuminate\Http\Request;

class CustomFieldCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('baongoc.customfieldcategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomFieldCategory  $customFieldCategory
     * @return \Illuminate\Http\Response
     */
    public function show(CustomFieldCategory $customFieldCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomFieldCategory  $customFieldCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomFieldCategory $customFieldCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomFieldCategory  $customFieldCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomFieldCategory $customFieldCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomFieldCategory  $customFieldCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomFieldCategory $customFieldCategory)
    {
        //
    }
}

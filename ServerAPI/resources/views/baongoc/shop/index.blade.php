@extends('layouts.admin')

@section('content')
	<!-- Begin page -->
	<div id="layout-wrapper">

	@include('layouts.header')

	@include('layouts.leftisdebar')

		<!-- ============================================================== -->
		<!-- Start right Content here -->
		<!-- ============================================================== -->
		<div class="main-content">

			<div class="page-content">
				<div class="container-fluid">

					<!-- start page title -->
					<div class="row">
						<div class="col-12">
							<div class="page-title-box d-sm-flex align-items-center justify-content-between">
								<h4 class="mb-sm-0 font-size-18">Shop</h4>

								<div class="page-title-right">
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">BaoNgoc</a></li>
										<li class="breadcrumb-item active">Shop</li>
									</ol>
								</div>

							</div>
						</div>
					</div>
					<!-- end page title -->

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="row mb-2">
										<div class="col-sm-4">

										</div>
										<div class="col-sm-8">
											<div class="text-sm-end">
												<a href="{{ url('baongoc/shop/create') }}" type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2"><i class="mdi mdi-plus me-1"></i> Add New Order</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-2.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-2.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-2.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-2.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
					</div>
					<!-- end row -->

					<div class="row">
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
						<div class="col-md-6 col-xl-3">

							<div class="card">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">Some quick example text to build on the card title and make
										up the bulk of the card's content.</p>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Cras justo odio</li>
									<li class="list-group-item">Dapibus ac facilisis in</li>
								</ul>
								<div class="card-body">
									<a href="#" class="card-link">Card link</a>
									<a href="#" class="card-link">Another link</a>
								</div>
							</div>

						</div><!-- end col -->
					</div>
					<!-- end row -->

					<div class="row">
						<div class="col-md-6 col-xl-3">
							<div class="card border border-primary">
								<div class="card-header bg-transparent border-primary">
									<h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow me-3"></i>Primary outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card border border-primary">
								<div class="card-header bg-transparent border-primary">
									<h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow me-3"></i>Primary outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card border border-primary">
								<div class="card-header bg-transparent border-primary">
									<h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow me-3"></i>Primary outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card border border-primary">
								<div class="card-header bg-transparent border-primary">
									<h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow me-3"></i>Primary outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
								</div>
							</div>
						</div>
					</div>
					<!-- end row -->

					<div class="row">
						<h4 class="my-3">Card groups</h4>
						<div class="col-md-6 col-xl-3">
							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-4.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">This is a longer card with supporting text below as
										a natural lead-in to additional content. This content is a little
										bit longer.</p>
									<p class="card-text">
										<small class="text-muted">Last updated 3 mins ago</small>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-4.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">This is a longer card with supporting text below as
										a natural lead-in to additional content. This content is a little
										bit longer.</p>
									<p class="card-text">
										<small class="text-muted">Last updated 3 mins ago</small>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-4.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">This is a longer card with supporting text below as
										a natural lead-in to additional content. This content is a little
										bit longer.</p>
									<p class="card-text">
										<small class="text-muted">Last updated 3 mins ago</small>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xl-3">
							<div class="card">
								<img class="card-img-top img-fluid" src="{{ asset('assets/images/small/img-4.jpg') }}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title mt-0">Card title</h4>
									<p class="card-text">This is a longer card with supporting text below as
										a natural lead-in to additional content. This content is a little
										bit longer.</p>
									<p class="card-text">
										<small class="text-muted">Last updated 3 mins ago</small>
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- end row -->

					<div class="row">
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class="avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-primary bg-soft text-primary font-size-16">
														B
													</span>
											</div>
											<h5 class="text-truncate pb-1">Brendle's</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>112</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$13,575</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-warning bg-soft text-warning font-size-16">
														T
													</span>
											</div>
											<h5 class="text-truncate pb-1">Tech Hifi</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>104</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$11,145</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-danger bg-soft text-danger font-size-16">
														L
													</span>
											</div>
											<h5 class="text-truncate pb-1">Lafayette</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>126</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$12,356</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-success bg-soft text-success font-size-16">
														P
													</span>
											</div>
											<h5 class="text-truncate pb-1">Packer</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>102</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$11,228</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-info bg-soft text-info font-size-16">
														N
													</span>
											</div>
											<h5 class="text-truncate pb-1">Nedick's</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>96</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$9,235</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-dark bg-soft text-dark text-light font-size-16">
														H
													</span>
											</div>
											<h5 class="text-truncate pb-1">Hudson's</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>120</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$14,794</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-dark bg-soft text-dark text-light font-size-16">
														T
													</span>
											</div>
											<h5 class="text-truncate pb-1">Tech Hifi</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>104</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$11,145</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-primary bg-soft text-primary font-size-16">
														B
													</span>
											</div>
											<h5 class="text-truncate pb-1">Brendle's</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>112</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$13,575</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="card">
								<div class="row">
									<div class="col-xl-5">
										<div class="text-center p-4 border-end">
											<div class=" avatar-sm mx-auto mb-3 mt-1">
													<span class="avatar-title rounded-circle bg-success bg-soft text-success font-size-16">
														L
													</span>
											</div>
											<h5 class="text-truncate pb-1">Lafayette</h5>
										</div>
									</div>

									<div class="col-xl-7">
										<div class="p-4 text-center text-xl-start">
											<div class="row">
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Products</p>
														<h5>126</h5>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<p class="text-muted mb-2 text-truncate">Wallet Balance</p>
														<h5>$12,356</h5>
													</div>
												</div>
											</div>
											<div class="mt-4">
												<a href="#" class="text-decoration-underline text-reset">See Profile <i class="mdi mdi-arrow-right"></i></a>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<!--  end row -->

				</div> <!-- container-fluid -->
			</div>
			<!-- End Page-content -->

		   @include('layouts.fotter')
		</div>
		<!-- ============================================================== -->
		<!-- end right Content here -->
		<!-- ============================================================== -->
	</div>
	<!-- END layout-wrapper -->

	<!-- Right bar overlay-->
	<div class="rightbar-overlay"></div>



@endsection

@section('js')
	<!-- Required datatable js -->
	<script src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<!-- Buttons examples -->
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
	<script src="{{ asset('assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
	<script src="{{ asset('assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>

{{--	<!-- Responsive examples -->--}}
{{--	<script src="{{ asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
{{--	<script src="{{ asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>--}}

{{--	<!-- Datatable init js -->--}}
{{--	<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>--}}
{{--	<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>--}}
{{--	<script src="{{ asset('assets/js/pages/tasklist.init.js') }}"></script>--}}
@endsection

@section('css')

@endsection


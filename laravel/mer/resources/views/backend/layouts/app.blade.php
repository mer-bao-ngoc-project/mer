<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @yield('meta')

    @yield('title')

    @yield('css')
</head>

<body data-sidebar="dark">

@yield('content')

</body>

</html>

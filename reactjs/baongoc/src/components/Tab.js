import React from 'react';

function Tab() {
    return (
        <div className="axil-tab-area axil-section-gap bg-color-white">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2 className="title">Top Stories</h2>
                        </div>
                    </div>
                </div>
                <div className="row mt--20 align-content-center">
                    <div className="col-lg-8 col-md-9 col-sm-12 col-12">
                        {/* Start Tab Button  */}
                        <ul className="axil-tab-button nav nav-tabs" id="axilTab" role="tablist">
                            <li className="nav-item" role="presentation">
                                <a className="nav-link active" id="tab-one" data-toggle="tab" href="#tabone" role="tab" aria-controls="tab-one" aria-selected="true">Travel</a>
                            </li>
                            <li className="nav-item" role="presentation">
                                <a className="nav-link" id="tab-two" data-toggle="tab" href="#tabtwo" role="tab" aria-controls="tab-two" aria-selected="false">Lifstyle</a>
                            </li>
                            <li className="nav-item" role="presentation">
                                <a className="nav-link" id="tab-three" data-toggle="tab" href="#tabthree" role="tab" aria-controls="tab-three" aria-selected="false">Accessory</a>
                            </li>
                            <li className="nav-item" role="presentation">
                                <a className="nav-link" id="tab-four" data-toggle="tab" href="#tabfour" role="tab" aria-controls="tab-four" aria-selected="false">Gadgets</a>
                            </li>
                        </ul>
                        {/* End Tab Button  */}
                    </div>
                    <div className="col-lg-4 col-md-3 col-sm-12 col-12">
                        <div className="see-all-topics text-left text-md-right mt_sm--20">
                            <a className="axil-link-button" >See All Topics</a>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        {/* Start Tab Content Wrapper  */}
                        <div className="tab-content" id="axilTabContent">
                            {/* Start Single Tab  */}
                            <div className="single-tab-content tab-pane fade show active" id="tabone" role="tabpanel" aria-labelledby="tab-one">
                                <div className="row">
                                    <div className="order-2 order-lg-1 col-lg-5 col-12 mt_md--40 mt_sm--40">
                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-01.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="POLICY">POLICY</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >TikTok pulls out of Hong
                                                    Kong due to new security law</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="APP">APP</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Slack is giving your
                                                    weekends back with per-day</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="GOOGLE">GOOGLE</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >How to personalize your
                                                    Google Chrome homepage with GIF</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/post-seo-list-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="BUSINESS">BUSINESS</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Chinese EV startups Byton
                                                    and Nio received paycheck </a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}
                                    </div>
                                    <div className="order-1 order-lg-2 col-lg-7 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-transparent post-overlay-bottom">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-post-06.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="BUSINESS">BUSINESS</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >If you can build a
                                                        business up big enough, it's respectable.</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab  */}
                            {/* Start Single Tab  */}
                            <div className="single-tab-content tab-pane fade" id="tabtwo" role="tabpanel" aria-labelledby="tab-one">
                                <div className="row">
                                    <div className="order-2 order-lg-1 col-lg-5 col-12 mt_md--40 mt_sm--40">
                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-01.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="POLICY">POLICY</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >TikTok pulls out of Hong
                                                    Kong due to new security law</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="APP">APP</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Slack is giving your
                                                    weekends back with per-day</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="GOOGLE">GOOGLE</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >How to personalize your
                                                    Google Chrome homepage with GIF</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/post-seo-list-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="BUSINESS">BUSINESS</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Chinese EV startups Byton
                                                    and Nio received paycheck </a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}
                                    </div>
                                    <div className="order-1 order-lg-2 col-lg-7 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-transparent post-overlay-bottom">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-post-06.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="BUSINESS">BUSINESS</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >If you can build a
                                                        business up big enough, it's respectable.</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab  */}
                            {/* Start Single Tab  */}
                            <div className="single-tab-content tab-pane fade" id="tabthree" role="tabpanel" aria-labelledby="tab-one">
                                <div className="row">
                                    <div className="order-2 order-lg-1 col-lg-5 col-12 mt_md--40 mt_sm--40">
                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-01.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="POLICY">POLICY</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >TikTok pulls out of Hong
                                                    Kong due to new security law</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="APP">APP</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Slack is giving your
                                                    weekends back with per-day</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="GOOGLE">GOOGLE</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >How to personalize your
                                                    Google Chrome homepage with GIF</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/post-seo-list-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="BUSINESS">BUSINESS</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Chinese EV startups Byton
                                                    and Nio received paycheck </a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}
                                    </div>
                                    <div className="order-1 order-lg-2 col-lg-7 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-transparent post-overlay-bottom">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-post-06.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="BUSINESS">BUSINESS</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >If you can build a
                                                        business up big enough, it's respectable.</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab  */}
                            {/* Start Single Tab  */}
                            <div className="single-tab-content tab-pane fade" id="tabfour" role="tabpanel" aria-labelledby="tab-one">
                                <div className="row">
                                    <div className="order-2 order-lg-1 col-lg-5 col-12 mt_md--40 mt_sm--40">
                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-01.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="POLICY">POLICY</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >TikTok pulls out of Hong
                                                    Kong due to new security law</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="APP">APP</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Slack is giving your
                                                    weekends back with per-day</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/liststyle-sm-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="GOOGLE">GOOGLE</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >How to personalize your
                                                    Google Chrome homepage with GIF</a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}

                                        {/* Start Single Post  */}
                                        <div className="content-block post-medium post-medium-border border-thin">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/small-images/post-seo-list-02.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-content">
                                                <div className="post-cat">
                                                    <div className="post-cat-list">
                                                        <a className="hover-flip-item-wrapper" >
                            <span className="hover-flip-item">
                              <span data-text="BUSINESS">BUSINESS</span>
                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h4 className="title"><a >Chinese EV startups Byton
                                                    and Nio received paycheck </a></h4>
                                            </div>
                                        </div>
                                        {/* Start Single Post  */}
                                    </div>
                                    <div className="order-1 order-lg-2 col-lg-7 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-transparent post-overlay-bottom">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-post-06.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="BUSINESS">BUSINESS</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >If you can build a
                                                        business up big enough, it's respectable.</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab  */}
                        </div>
                        {/* End Tab Content Wrapper  */}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Tab;
